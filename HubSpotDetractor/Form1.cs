﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Windows.Forms;
using static HubSpotDetractor.Program;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace HubSpotDetractor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback =
             delegate (object s, X509Certificate certificate,
                      X509Chain chain, SslPolicyErrors sslPolicyErrors)
             { return true; };
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            try
            {
                //Create file name for output
                Dictionary<string, int> dictionary = new Dictionary<string, int>();

                Detractor connection = new Detractor();

                string vDate;

                //Day of week to pull the list from
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                { vDate = DateTime.Now.AddDays(-3).ToShortDateString(); }
                else { vDate = DateTime.Now.AddDays(-1).ToShortDateString(); }


                var connetionString = ConfigurationManager.ConnectionStrings["Application_DatabaseConnection"].ConnectionString;
                SqlConnection sqlConnection = new SqlConnection(connetionString);

                DataTable sqlDt = new DataTable();

                string sqlSelect = "SELECT CC.FriendlyId CLTID, CN.FirstName FNAME, CN.LastName LNAME, CE.Email EMAIL, sd.DetractorScore SA42, SD.SentDate SDTE, CC.Reference as Reference " +
                                 " FROM client.contract as CC" +
                                 " join client.contractapplicant as cap on cap.contractid = CC.id" +
                                 " join Client.Name as CN on CN.ClientId = cap.clientid" +
                                 " join Client.Email as CE ON CE.clientid = cap.ClientId" +
                                 " join Client.Detail as CD on CD.Id = cap.clientid" +
                                 " join survey.SurveyDetails SD ON SD.ContractId = CC.Id" +
                                 " left outer join client.CancelDetails as CND on CND.ContractId = cap.ContractId" +
                                 " WHERE CND.ContractId IS NULL AND FORMAT(SD.CompletedDate, 'd', 'en-us') = '" + (Convert.ToDateTime(vDate)).ToShortDateString() +
                                 "' AND sd.DetractorResponse = 'Detractor'" + //"' AND sd.DetractorScore Between 0 AND 6 " +
                                 " AND CE.EmaiL LIKE '%@%.%' AND cd.HubspotOptOut = '0'";
                              //" AND left(CC.FriendlyId,3) > 800 and left(CC.FriendlyId,3) < 899 ";
                              //" AND CC.FriendlyId in (36110668,36110669,36110670,36110671) ";
                
                //need to pull clients that filled out the survey yesterday.

                SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                //DataTable sqlDt = new DataTable();HubspotOptOut
                sqlDa.Fill(sqlDt);
                //loop through and write lines to the file
                if(sqlDt.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt.Rows[0].Table.Rows)
                    {

                        var clientNotes = "(WelcomeLetterFDR Sent),(Century LCM email sent, from the CEO, to client Welcoming them to the Century debt settlement program)";
                        sqlConnection = new SqlConnection(connetionString);
                        InsertClientNotes(Convert.ToInt32(row["CLTID"]), row["Reference"].ToString(), clientNotes, sqlConnection);

                        string sqlUpdate2 = "UPDATE hubspot.HSCLIENT SET hubspot.HSCLIENT.DETRACTORWG = '" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString() + "', hubspot.HSCLIENT.DETRACTORSS = '" + row["SA42"] + "' " +
                              "WHERE hubspot.HSCLIENT.CLTID = " + row["CLTID"];
                        sqlConnection = new SqlConnection(connetionString);
                        sqlConnection.Open();
                        SqlCommand myCommand1 = new SqlCommand(sqlUpdate2, sqlConnection);
                        myCommand1.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                }


                string sqlSelect2 = "SELECT HS.CLTID, HS.HSVID, HS.DETRACTORWG, HS.DETRACTORSS " +
                                    "FROM hubspot.HSCLIENT HS " + "WHERE FORMAT(HS.DETRACTORWG, 'd', 'en-us') = '" + DateTime.Today.ToShortDateString() + "'";
                            //" AND left(HS.CLTID,3) > 800 and left(HS.CLTID,3) < 899 "; ;
                            //" AND HS.CLTID in (36110668,36110669,36110670,36110671) "; ;
                sqlConnection = new SqlConnection(connetionString);
                sqlConnection.Open();
                SqlCommand sqlCommand1 = new SqlCommand(sqlSelect2, sqlConnection);

                SqlDataAdapter sqlDa1 = new SqlDataAdapter(sqlCommand1);
                DataTable sqlDt1 = new DataTable();
                sqlConnection.Close();
                sqlDa1.Fill(sqlDt1);
                if(sqlDt1.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt1.Rows[0].Table.Rows)
                    {
                        string HSVID = row["HSVID"].ToString();
                        string DETRACTORSS = row["DETRACTORSS"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + HSVID + "/profile?property=detractor_survey_date");

                        connection.DetractorURL = url;

                        connection.GetDETRACTORSS = DETRACTORSS;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.DetractorUpdate();
                    }
                }
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                send_email("Could not build Detractor File", ex.Message);
            }
        }

        public void send_email(string error_message,string error_string)
        {

            Outlook.Application oApp = new Outlook.Application();
            Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

            Outlook.Recipient oRecip = (Outlook.Recipient)oMsg.Recipients.Add("dailytasks@centuryss.com");
            oRecip = (Outlook.Recipient)oMsg.Recipients.Add("rogerc@centuryss.com");
            oRecip = (Outlook.Recipient)oMsg.Recipients.Add("bvasquez@centuryss.com");
            oRecip.Resolve();

            oMsg.Subject = error_message;
            oMsg.HTMLBody += "<HTML><BODY></BR><P>" + error_message + "</P></BR></BR><P>" + error_string + "</P></BR></BODY></HTML>";
            oMsg.Save();
            oMsg.Send();

            oRecip = null;
            oMsg = null;
            oApp = null;

        }

        public static void InsertClientNotes(int contractId, string reference, string notes, SqlConnection sql_connection)
        {
            using (SqlCommand cmd = new SqlCommand("[audit].[AuditContractChange]", sql_connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userId", 1);
                cmd.Parameters.AddWithValue("@contractId", null);
                cmd.Parameters.AddWithValue("@reference", reference);
                cmd.Parameters.AddWithValue("@changeTypeId", 7);
                cmd.Parameters.AddWithValue("@notes", notes);

                DataTable fieldDetails = CreateUserContractChangeDetailTable();
                cmd.Parameters.AddWithValue("@fieldDetails", fieldDetails);
                cmd.Parameters.AddWithValue("@loanId", null);
                sql_connection.Open();
                cmd.ExecuteNonQuery();
                sql_connection.Close();
            }
        }

        public static DataTable CreateUserContractChangeDetailTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FieldId", typeof(Byte));
            dt.Columns.Add("OldValue", typeof(string));
            dt.Columns.Add("NewValue", typeof(string));
            return dt;
        }
    }
}
