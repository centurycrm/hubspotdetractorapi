﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotDetractor
{
    public enum httpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    class Detractor
    {
        public string DetractorURL { get; set; }
        public string GetDETRACTORSS { get; set; }

        public string DetractorUpdate()
        {
            //Grabs todays date and converts it to milliseconds because thats how HS reads in dates
            var date = (DateTime.Now.Month.ToString("d2") + "/" + DateTime.Now.Day.ToString("d2") + "/" + DateTime.Now.Year.ToString("d4"));
            var datecon = Convert.ToDateTime(date);
            var today = (datecon - new DateTime(1970, 1, 1)).TotalMilliseconds;

            string strResponseValue = string.Empty;

            strResponseValue = "{\"properties\":[{\"property\": \"detractor_survey_date\",\"value\": \"" + today + "\"}]}"; //,{\"property\": \"survey_score\",\"value\": \"" + GetDETRACTORSS + "\"}]}";
            string AccessToken = "pat-na1-b85c99b7-2c6e-4eb7-a0c1-4953f98528bb";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(DetractorURL);
            request.Headers.Add("Authorization", "Bearer " + AccessToken);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "POST";
            request.Accept = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(strResponseValue);
            }
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {       //If it's a 200 or a 204
                    if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.NoContent)
                    {
                        try
                        {
                            throw new ApplicationException("error code:" + response.StatusCode.ToString());
                        }
                        catch (ApplicationException et)
                        {
                            throw new ApplicationException("error code:" + et.Message);
                        }
                    }
                    //Process the response stream... (could be JSON, XML or HTML ect...)

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                strResponseValue = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        strResponseValue = sr.ReadToEnd();
                    }
                }
            }
            
            return strResponseValue;
        }

    }
}
